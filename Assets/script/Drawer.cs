﻿using UnityEngine;
using System.Collections;

public class Drawer : MonoBehaviour {
	private Transform tr;
	public GameObject prefab;

	public static float level = 3f;

	private Quaternion q;

	private float timing = 0f;

	private float random = 0f;

	// Use this for initialization
	void Start () {
		tr = GetComponent<Transform>();
		random = Random.Range(-6f, 6f);
		tr.position = new Vector3(random, 4f);
	}
	
	// Update is called once per frame
	void Update () {
		if ((timing += 1 * Time.deltaTime) >= 3f) {
			q=Quaternion.Euler(0f, 0f, Random.Range(-80f, 80f));
			timing = Random.Range(0.0f,1.0f);
			random = Random.Range(-6f, 6f);
			tr.position = new Vector3(random, 4f);
			Debug.Log(tr.position + "" + random + "" + " q = " + q);
			Instantiate(prefab, tr.position, q);
		}
	}

	public static float GetLevel() {
		return level;
	}
}
