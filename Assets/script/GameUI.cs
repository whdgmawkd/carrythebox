﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameUI : MonoBehaviour {
	private int score = 0;
	public Text txtScore;
	public GameObject Cross;
	public GameObject SWall;
	public GameObject BoxDrawer;
	public GameObject BombDrawer;
	public static int heart = 3;
	public GameObject canvas;

	private int LvUp = 0;
	private float LvUpTM = 0f;
	private float tDelta = 0f;
	public static bool isLotate = false;
	public static bool isGameStarted = false;

	// Use this for initialization
	void Start () {
		DispScore(0);
		EndGame();

	}
	
	// Update is called once per frame
	void Update () {
		if (score < 0 || heart < 1) {
			EndGame();
		}
		DispScore(0);
		if ((tDelta += Time.deltaTime) >= 2) {
			tDelta = 0f;
			if (Drawer.GetLevel() > 1)
				Drawer.level -= 0.05f;
		}
		ChangeLevel(Drawer.GetLevel());
	}

	void ChangeLevel(float gameLevel) {
		switch (string.Format("0.00", gameLevel)) {
			case "2.50":
				addDrawer(BombDrawer, 1);
				break;
			case "2.00":
				addDrawer(BoxDrawer, 1);
				break;
			case "1.50":
				break;
			case "1.00":
				break;
		}
	}

	IEnumerator HideCanvas() {
		CanvasGroup canvasGroup = canvas.GetComponent<CanvasGroup>();
		while (canvasGroup.alpha > 0f) {
			canvasGroup.alpha -= Time.deltaTime / 2;
		}
		yield return null;
	}

	IEnumerator ShowCanvas() {
		CanvasGroup canvasGroup = canvas.GetComponent<CanvasGroup>();
		while (canvasGroup.alpha < 1f) {
			canvasGroup.alpha += Time.deltaTime / 2;
		}
		yield return null;
	}
/*
	public void HideCanas() {
		CanvasGroup canvasGroup = canvas.GetComponent<CanvasGroup>();
		while (canvasGroup.alpha > 0f)
			canvasGroup.alpha -= Time.deltaTime / 2;
		canvasGroup.alpha = 0f;
	}

	public void ShowCanvas() {
		CanvasGroup canvasGroup = canvas.GetComponent<CanvasGroup>();
		while (canvasGroup.alpha < 1f)
			canvasGroup.alpha += Time.deltaTime / 2;
		canvasGroup.alpha = 1f;
	}
	*/
	public void DispScore(int accScore) {
		if(score <= 0 && accScore < 0) {
			DelHeart();
			return;
		}
		score += accScore;
		txtScore.text = "SCORE <color=#ff0000>" + score.ToString() + "</color>" +
			"\n Level : " + string.Format("{0:F2}", Drawer.GetLevel()) +
			"\n Heart : " + heart;
	}

	public void InitScore() {
		score = 0;
		LvUp = 0;
		LvUpTM = 0f;
		heart = 3;
		txtScore.text = "SCORE <color=#ff0000>" + score.ToString() + "</color>" +
						"\nLevel : " + string.Format("{0:F2}", Drawer.GetLevel()) +
						"\nHeart : " + heart;
	}

	public static void DelHeart() {
		heart--;
	}

	public void EndGame() {
		foreach (GameObject obj in GameObject.FindGameObjectsWithTag("BOXDRAWER")) {
			Destroy(obj);
		}
		foreach(GameObject obj in GameObject.FindGameObjectsWithTag("BOMBDRAWER")) {
			Destroy(obj);
		}
		foreach (GameObject obj in GameObject.FindGameObjectsWithTag("OBSTACLE")) {
			Destroy(obj);
		}
		StartCoroutine(ShowCanvas());
		isGameStarted = false;
	}

	public void InitGame() {
		InitScore();
		addDrawer(BoxDrawer, 2);
		addDrawer(BombDrawer, 3);
		isGameStarted = true;
		StartCoroutine(HideCanvas());
		Drawer.level = 3;
	}
	public void addDrawer(GameObject obj, int count) {
		int a;
		for (a = 0; a < count; a++) {
			Instantiate(obj);
		}
	}
}
