﻿using UnityEngine;
using System.Collections;

public class CatcherCtrl : MonoBehaviour {

	private int score = 0;
	private GameUI gameUI;

	private float moveSpeed = 12f;

	private Transform tr;

	// Use this for initialization
	void Start() {
		tr = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
		if ((transform.position.x <= -6f && Input.GetAxis("Horizontal") < 0f) ||
			(transform.position.x >= 6f && Input.GetAxis("Horizontal") > 0f) && GameUI.isGameStarted) {

		}
		else if (GameUI.isGameStarted) {
			tr.Translate(Vector3.right * Input.GetAxis("Horizontal") * moveSpeed * Time.deltaTime);
		}
	}

	void OnCollisionEnter(Collision coll) {
		gameUI = GameObject.Find("GameUI").GetComponent<GameUI>();
		if (coll.collider.tag == "BOX") {
			Destroy(coll.gameObject);
			Debug.Log(score);
			gameUI.DispScore(10);
		}
		if (coll.collider.tag == "BOMB") {
			Destroy(coll.gameObject);
			gameUI.DispScore(-5);
			if(Drawer.GetLevel() < 1) {
				GameUI.DelHeart();
			}
		}
	}
}
