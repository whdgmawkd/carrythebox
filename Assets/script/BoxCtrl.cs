﻿using UnityEngine;
using System.Collections;

public class BoxCtrl : MonoBehaviour {

	private Transform tr;
	private Rigidbody rb;

	public float level = 2f;

	// Use this for initialization
	void Start () {
		tr = GetComponent<Transform>();
		rb = GetComponent<Rigidbody>();
		rb.AddForce(Vector3.down * level);
		level += 0.5f * Time.deltaTime;
	}
	
	// Update is called once per frame
	void Update () {
		//tr.Translate(Vector3.down * level * Time.deltaTime);

	}
}
